import numpy as np
import pandas as pd
from scipy.sparse import lil_matrix
from tqdm.auto import tqdm
import itertools
from sklearn.preprocessing import normalize as sklearn_normalize


def transitive_labeling(df:pd.DataFrame, event_id_column: str, object_columns: list, labels: dict):
    all_columns = [event_id_column] + object_columns

    object_list, object_index = get_object_index(df, all_columns)
    label_list, label_index = get_label_index(labels)
    v = label_dict_to_vector(labels, object_index, label_index)
    A = get_adjecency_matrix_from_df(df, all_columns, object_index)

    v = message_passing_loop(A, v)

    label_members = get_label_members_from_vector(v, label_list, label_index, object_list)
    split_label_members = get_split_label_members(label_members, df, event_id_column)
    return split_label_members


def get_object_index(df: pd.DataFrame, object_columns: list):
    object_list = pd.unique(df[object_columns].values.ravel("K"))
    object_list = object_list[~pd.isnull(object_list)]
    object_index = {object_name: object_index for object_index, object_name in enumerate(object_list)}
    return object_list, object_index


def get_label_index(label_dict):
    label_list = tuple(set(label_dict.values()))
    label_index = {label: label_index for label_index, label in enumerate(label_list)}
    return label_list, label_index


def get_adjecency_matrix_from_df(df: pd.DataFrame, object_columns: list, object_index: dict):
    A = lil_matrix((len(object_index), len(object_index)))

    for _, row in tqdm(df[object_columns].iterrows(), total=len(df), desc="Building adjecency matrix"):
        row_objects = row.values
        row_objects = row_objects[~pd.isnull(row_objects)]
        edges = itertools.permutations(np.vectorize(object_index.get)(row_objects), 2)
        x_indizes, y_indizes = zip(*edges)
        A[x_indizes, y_indizes] = 1
    return A


def label_dict_to_vector(label_dict: dict, object_index: dict, label_index: dict):
    label_vector = lil_matrix((len(object_index), len(label_index)))

    for object_name, label in tqdm(label_dict.items(), desc="Building label vector"):
        label_vector[object_index[object_name], label_index[label]] = 1
    return label_vector


def message_passing_forward(A, vector_in):
    u = A * vector_in
    # Create mask to only update vector rows that are zero (unlabeled)
    mask = (vector_in * np.ones(vector_in.shape[1])).astype(bool)
    u_normed = sklearn_normalize(u, norm="l1", axis=1).tolil()  # TODO: verify that axis=1 is correct
    u_normed[mask] = 0
    return vector_in + u_normed


def message_passing_loop(A, v):
    n_labels_old = 0
    n_labels_new = v.count_nonzero()
    loop_counter = 1
    while n_labels_new > n_labels_old:
        print(f"Iteration {loop_counter}: {n_labels_new} labels found")
        v = message_passing_forward(A, v)
        n_labels_old = n_labels_new
        n_labels_new = v.count_nonzero()
        loop_counter += 1
    return v


def get_label_members_from_vector(label_vector, label_list, label_index, object_list, threshold=None):
    label_members = {}
    eye = np.eye(len(label_list))
    for label in label_list:
        label_slice = label_vector * eye[label_index[label]]
        if threshold is None:
            mask = label_slice > 0
        else:
            mask = label_slice > threshold
        label_members[label] = set(object_list[np.where(mask)])
    return label_members


def get_split_label_members(label_members, df, event_id_column):
    event_ids = set(df[event_id_column].dropna().unique())
    event_label_members = {}
    object_label_members = {}

    for label in label_members:
        event_label_members[label] = label_members[label].intersection(event_ids)
        object_label_members[label] = label_members[label] - event_ids

    return {"events": event_label_members, "objects": object_label_members}

