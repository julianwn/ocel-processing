import pandas as pd
from pm4py.objects.ocel.obj import OCEL
import pm4py


def ocel_filter_variants_percentage(ocel: OCEL, percentage_dict: dict):
    """
    Filters the OCEL on multiple object types by most frequent variants

    ocel: OCEL object containing event df, object df and relations df,
    percentage_dict: object_type => percentage
    """
    # ocel_object_types = list(OCEL.object_type_columns.unique())
    frequent_trace_objects = {}
    for object_type in percentage_dict.keys():
        flattened_ocel = pm4py.ocel_flattening(ocel, object_type)
        log = pm4py.convert_to_event_log(flattened_ocel)
        filtered_log = pm4py.filter_variants_percentage(log, percentage_dict[object_type])
        filtered_df = pm4py.convert_to_dataframe(filtered_log)
        frequent_trace_objects[object_type] = set(filtered_df[ocel.object_id_column].unique())

    new_ocel = ocel.__deepcopy__()

    # only keep frequent variants in new_ocel.relations, events and objects
    for object_type, frequent_variants in frequent_trace_objects.items():
        # Drop rows with current object type where object_id is not in frequent variants
        new_ocel.relations.drop((new_ocel.relations[new_ocel.object_type_column] == object_type)
                                & ~new_ocel.relations[new_ocel.object_id_column].isin(frequent_variants))

    return new_ocel

