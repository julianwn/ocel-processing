import pandas as pd
import numpy as np
from pandas.api.types import is_datetime64_dtype
from tqdm.auto import tqdm


def build_events_dict(input_df: pd.DataFrame, object_columns: list, timestamp_col_name="timestamp",
                      activity_col_name="activity"):
    """ Create events dict from Dataframe. Expects np.nan in object columns as N/A-value
        ocel:vmap not implemented yet
    """
    df = input_df.copy()

    df["ocel:omap"] = [[el for el in row if el is not np.nan] for row in
                       tqdm(df[object_columns].values, desc="Building events dict")]
    df.drop(object_columns, axis=1, inplace=True)
    df["ocel:vmap"] = [{"items": {}} for row in range(len(df))]

    if is_datetime64_dtype(df[timestamp_col_name]):
        df[timestamp_col_name] = [t.isoformat() for t in tqdm(df[timestamp_col_name], desc="Converting timestamps")]

    df.rename(
        columns={
            timestamp_col_name: "ocel:timestamp",
            activity_col_name: "ocel:activity"
        },
        inplace=True
    )
    return df.to_dict(orient="index")


def build_header_dict(object_columns: list, attribute_columns=None, ocel_version="1.0", ocel_ordering="timestamp"):
    if attribute_columns is None:
        attribute_columns = []

    return {
        "ocel:version": ocel_version,
        "ocel:ordering": ocel_ordering,
        "ocel:object-types": object_columns,
        "ocel:attribute-names": attribute_columns
    }


def build_object_dict_from_df(df: pd.DataFrame, object_columns: list):
    """ Fast method to create object dict but without the ability to gather additional object variables
        Expects np.nan as N/A value
    """
    object_dict = {}

    for object_type in tqdm(object_columns, desc="Building object dict"):
        object_set = set(df[object_type].unique())

        if np.nan in object_set:
            object_set.remove(np.nan)

        new_object_dict = {
            object_name: {
                "ocel:type": object_type,
                "ocel:ovmap": {}
            }
            for object_name in object_set
        }

        object_dict.update(new_object_dict)
    return object_dict


def build_ocel_dict(df: pd.DataFrame, object_columns: list):
    ocel_dict = {
        "ocel:global-log": build_header_dict(object_columns),
        "ocel:global-event": {},
        "ocel:global-object": {},
        "ocel:events": build_events_dict(df, object_columns),
        "ocel:objects": build_object_dict_from_df(df, object_columns)
    }
    return ocel_dict
